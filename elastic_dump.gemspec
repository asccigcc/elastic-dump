# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name        = 'g2_elastic_dump'
  spec.version     = '0.0.1'
  spec.authors     = ['Pastorinni Ochoa']
  spec.email       = ['pastorinni.ochoa@verisk.com']

  spec.summary     = %q{Dump ES command}
  spec.description = %q{Provide a tools to dump information from ES}

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'thor', '0.20.0'
  spec.add_dependency 'dry-container', '~> 0.6'
  spec.add_dependency 'elasticsearch', '~> 6.0'

  spec.add_development_dependency 'bundler', '~> 1.16.0'
  spec.add_development_dependency 'rake', '~> 12.0.0'
  spec.add_development_dependency 'minitest', '~> 5.10.2'
  spec.add_development_dependency 'pry', '0.10.4'
end
