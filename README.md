# ElasticDump command line tool

This is a command line tool that it helps to dump indices from an input to output.
The script uses [TaskRabbit](https://github.com/taskrabbit/elasticsearch-dump) tool under the hood but integrates some improvements in order to support many indices.

### Local installation

You require to have installed node, for instance in MacOS we can use brew:

```
$ brew install node
```

Or just download the package from the website:

[Node](https://nodejs.org/en/)

After the node installation, we have to run the node package manager. This command will install [elasticsearch-dump](https://github.com/taskrabbit/elasticsearch-dump)

```
$ npm install
```

After the installation we, are ready to run bundler for ruby.

```
$ bundle install
```

When all the packages are installed we have to add permissions to the g2-elasticdump command.

```
$ chmod a+x exe/g2_elastic_dump
```

After this last step, we have to create a copy for config.yml.example to config.yml and then edit. The file contains the indices and the paths that we require in order to dump the information. Go to configuration section and see more details.

## Configuration

The command line has a config.yml file, here is where we declare the indices that we want to dump and the source and destiny path.

We have two sections one is the input and the other the output indices. The input is the elastic server where we want to get the information and the output is where we want to put the information.

This is an example of dump locally:

```
  input_indices:
    url: pmm-elastic.server-qa
    port: 9200
    scheme: https
    user: es_admin
    password: pass
    full_index: pmm_monthly_audit_full_qa_index
    simple_index: pmm_monthly_audit_simple_qa_index
    es_env_index: qa
  output_indices:
    url: localhost
    port: 9200
    scheme: http
    full_index: pmm_monthly_audit_full_local_index
    simple_index: pmm_monthly_audit_simple_local_index
    es_env_index: local
  filters:
    years:
      - 2017
      - 2018

```

We can use user and password if is required in input or output.

## Use

For the moment the script only supports 3 commands: `analyzer`, `mapping`, and `data`. In addition, the script support the `complete` command, which will run `analyzer`, `mapping`, and `data` on a single execution. Do note, the script only dumps into another Elasticsearch server (it is not possible to export for the moment into a different format).

You can execute the next command:

```
./exe/g2_elastic_dump dump --type 'mapping'
```

Where type is the operation that we an to execute. Here is a list of operations:

- Analyzer (analyzer)
- Data (data)
- Mapping (mapping)
- Complete (analyzer/mapping/data)
- Template (template)
- Delete (delete): This operation delete all the indices from destiny source.

### Use Filter regex pattern

In order to filter the indices that we want to keep and dump we have the filter attribute. You only have to include when you are calling the command line in the following way:

```
./exe/g2_elastic_dump dump --type 'mapping' --filter 'years'
```

The filter is defined into the config file in order to have to define own filters. We have to declare in this way:

```
# config.yml

....
  filters:
    years:
      - 2017
      - 2018
```

The command line is gonna do a regex validation using the listed strings in this case 2017 and 2018. You have to notice that you can declare your own filter name and string list.

### Mapping
As part of the `mapping` command, it is required to specify the `mapping_type` on the `config.yml` file. A JSon file with the same name needs to be provided on the config folder in order to execute correctly.

For example: when setting up the mapping type as `someMappingType`, a `someMappingType.json` (which contains the mapping properties) is expected.

```
# config.yml

...
filters:
  custom:
    - "2017_04"
mapping_type: someMappingType  ## Mapping type configuration, in order to run mapping/complete
...
```

### Template
Template works slightly different from the other commands. It doesn't work with indices directly, but instead it creates a template on the configuration that is used by ES to create new indices with that configuration

`./exe/g2_elastic_dump dump --type 'template' --template 'sample_option'`

It is required to specify the `templates` section on the `config.yml` file. Additionally, it is also required to place a JSon file on the `config` folder with the specified name.

For example: when setting up the config options for `sample_option`:
- `name` will be the name of the template
- `file` will be the filename of the JSon that the application will look for

```
# config.yml

...
filters:
  custom:
    - "2017_04"
templates:
  sample_option:    ## parameter chosen on the call
    name: "monthly_report"
    file: "example_file.json"
...
```

**Note:** Templates are only needed to be uploaded once. More information can be found on https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-templates.html

## ToDo

- Support all the commands and options from elascticsearch-dump.
- Implement a validator of ES versions in order to return warning in case of incompatibilities (like mapper).
- Support more indices instead of full and simple.
- See the possibility to remove the dependency to elasticsearch-dump.
- Integrate threads in order to do faster the dump process.
