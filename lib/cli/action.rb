module ElasticDump
  class Action
    def self.run(container, options = {})
      service = new(container, options)
      service.perform
    end

    attr_accessor :container, :logger, :config, :options, :command_type

    def initialize(container, options)
      @container    = container
      @logger       = @container.resolve(:logger)
      @config       = @container.resolve(:config)
      @options      = options
      @command_type = container.resolve(:command_type)
    end

    def perform
      raise 'Implement your code into action class'
    end

    private
    def logger(message)
      @logger.info(message)
    end
  end
end
