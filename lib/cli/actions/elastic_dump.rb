module ElasticDump
  class ElasticDump < Action
    def perform
      logger('Starting Dump process')

      if container[:filter]
        indices_input    = GetIndices.run(container)
        indices_filtered = FilterIndices.run(container, indices_input)
        indices          = PrepareIndices.run(container, indices_filtered)

        send(command_type, indices)
      end

      if container[:template]
        template_input = GetTemplate.run(container)

        send(command_type, template_input)
      end
    end

    private
    def mapping(indices)
      Mapping.run(container, indices)
    end

    def data(indices)
      Data.run(container, indices)
    end

    def analyzer(indices)
      Analyzer.run(container, indices)
    end

    def delete(indices)
      Delete.run(container, indices)
    end

    def complete(indices)
      Complete.run(container, indices)
    end

    def template(template)
      Template.run(container, template)
    end
  end
end
