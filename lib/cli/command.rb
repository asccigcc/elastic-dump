module ElasticDump
  class Command
    def self.run(container, options = {})
      service = new(container, options)
      service.perform
    end

    attr_accessor :container, :logger, :config, :options, :filter

    def initialize(container, options)
      @container = container
      @logger    = container.resolve(:logger)
      @config    = container.resolve(:config)
      @filter    = container.resolve(:filter)
      @options   = options
    end

    def perform
      raise 'Implement your code into command class'
    end

    private
    def logger(message)
      @logger.info(message)
    end

    # @TODO We have to remove the dependency from elasticdump and create ours.
    # Please read the following post: https://github.com/taskrabbit/elasticsearch-dump/issues/333
    def prepare_command(indices, type)
      command(indices, type)
    end

    def host_path_for(indices_source)
      host_attr = config[indices_source]
      "#{host_attr['scheme']}://#{auth_keys(host_attr)}#{host_attr['url']}:#{host_attr['port']}"
    end

    def auth_keys(host_attr)
      if host_attr.has_key?("user") && host_attr.has_key?("password")
        "#{host_attr['user']}:#{host_attr['password']}@"
      end
    end

    def execute_command(command)
      logger("Executing command: #{command}")
      system(command)
    end

    def command(indices, type)
      <<-COMMAND
          NODE_TLS_REJECT_UNAUTHORIZED=0 ./node_modules/elasticdump/bin/elasticdump \
            --input='#{host_path_for('input_indices')}/#{indices.first}' \
            --output='#{host_path_for('output_indices')}/#{indices.last}' \
            --type='#{type}'
      COMMAND
    end
  end
end
