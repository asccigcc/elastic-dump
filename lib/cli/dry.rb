module ElasticDump
  module Dry
    class << self
      def initialize_container
        @container ||= ::Dry::Container.new
      end

      def register(name, klass)
        container.register(name, klass)
      end
    end
  end
end
