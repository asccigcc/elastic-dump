module ElasticDump
  class Analyzer < Command
    def perform
      logger('Analyze indices')

      options.each do |indices|
        logger("Running index: #{indices}")
        command = prepare_command(indices, :analyzer)
        execute_command(command)
      end
    end
  end
end
