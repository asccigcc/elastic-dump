module ElasticDump
  class Mapping < Command
    def perform
      logger('Mapping indices')

      mapping_type = config["mapping_type"]
      mapping_path = "#{File.dirname(File.dirname(File.dirname(File.dirname(File.absolute_path(__FILE__)))))}/config/#{mapping_type}.json"
      mapping_body = File.read(mapping_path)

      logger("mapping read from #{mapping_path}")

      options.each do |indices|
        logger("Running index: #{indices}")
        execute_command(command(indices, mapping_type, mapping_body))
      end
    end

    private
    def command(indices, mapping_type, mapping_body)
      <<-COMMAND
          curl -XPUT -k '#{host_path_for('output_indices')}/#{indices.last}/_mapping/#{mapping_type}' \
            --header "Content-Type: application/json" \
            --data '#{mapping_body}'
      COMMAND
    end
  end
end
