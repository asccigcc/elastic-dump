module ElasticDump
  class PrepareIndices < Command
    def perform
      logger('Prepare Indices to dump')

      options.map do |input_index|
        [input_index, output_index_from(input_index)]
      end
    end

    private
    def output_index_from(input_index)
	if input_index =~ /simple/
		input_index.gsub(config['input_indices']['simple_index'], config['output_indices']['simple_index'])
	else
		input_index.gsub(config['input_indices']['full_index'], config['output_indices']['full_index'])
	end
    end
  end
end
