module ElasticDump
  class Template < Command
    def perform
      logger('Create template')

      template = options

      execute_command(command(template))
    end

    private
    def command(template)
      <<-COMMAND
          curl -XPUT -k '#{host_path_for('output_indices')}/_template/#{template[:name]}' \
            --header "Content-Type: application/json" \
            --data '#{template[:payload]}'
      COMMAND
    end
  end
end
