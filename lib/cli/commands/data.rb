module ElasticDump
  class Data < Command
    def perform
      logger('Data indices')

      options.each do |indices|
        logger("Running index: #{indices}")
        command = prepare_command(indices, :data)
        execute_command(command)
      end
    end
  end
end
