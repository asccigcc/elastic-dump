module ElasticDump
  class Complete < Command
    def perform
      logger('Analyze, Mapping, and Data indices')

      indices = options

      Analyzer.run(container, indices)
      Mapping.run(container, indices)
      Data.run(container, indices)
    end
  end
end
