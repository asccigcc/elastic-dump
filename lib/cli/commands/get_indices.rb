module ElasticDump
  class GetIndices < Command
    def perform
      logger('Get indices')
      indices_hash = client.indices.get_aliases
      return_indices_list(indices_hash)
    end

    protected
    def input_url
      host_path = config['input_indices']

      [
        {
          host: host_path['url'],
          port: host_path['port'],
          user: host_path['user'],
          password: host_path['password'],
          scheme: host_path['scheme']
        }
      ]
    end

    def client
      @client ||= Elasticsearch::Client.new hosts: input_url, log: true, transport_options: { ssl: { verify: false }}
    end

    def return_indices_list(indices_hash)
      indices_list = []
      indices_hash.keys.each { |key| indices_list << key if key =~ /#{config['input_indices']['es_env_index']}/ }
      indices_list
    end
  end
end
