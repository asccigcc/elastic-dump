module ElasticDump
  class FilterIndices < Command
    def perform
      logger('Filter indices')
      return_indices_list(options) unless filter.nil?
    end

    protected
    def return_indices_list(indices_list)
      filter_regex     = config["filters"][filter].join('|')
      indices_list.select { |key| key =~ /#{filter_regex}/ }
    end

  end
end
