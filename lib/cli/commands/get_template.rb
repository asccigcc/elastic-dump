module ElasticDump
  class GetTemplate < Command
    def perform
      logger('Get template')

      input_template
    end

    protected
    def input_template
      parameter = container[:template]
      templates_config = config['templates'][parameter]

      filename = templates_config['file']

      payload = get_payload(filename)

      @input_template = {
          name: templates_config['name'],
          file: filename,
          payload: payload
        }
    end

    private
    def get_payload(filename)
      config_folder = "#{File.dirname(File.dirname(File.dirname(File.dirname(File.absolute_path(__FILE__)))))}/config/"

      template_path = "#{config_folder}#{filename}"

      template_file = File.read(template_path)

      # @# TODO: Add JSon validation(-?)
      #json_file = MultiJson.load(template_file)
    end
  end
end
