module ElasticDump
  class Delete < Command
    def perform
      logger('Delete indices')

      options.each do |indices|
        logger("Running index: #{indices}")
        execute_command(command(indices))
      end
    end

    private
    def command(indices)
      "curl -XDELETE -k '#{host_path_for('output_indices')}/#{indices.last}?pretty'"
    end
  end
end
