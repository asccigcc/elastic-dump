module ElasticDump
  class Main < Thor
    desc 'dump', 'Dump an elasticsearch indices from config file'
    option :type, :type => :string
    option :config, :type => :string
    option :env, :type => :string
    option :filter, :type => :string
    option :template, :type => :string

    def dump
      puts 'ElasticSearch Dump'
      extract_input_from(options)
      initialize_system
      elastic_dump
    end

    desc 'reindexing', 'Reindexing indices from the ES cluster'
    option :config, :type => :string
    option :env, :type => :string
    option :filter, :type=> :string

    def reindexing
      puts 'Reindexing ES indices'
    end

    private
    attr_accessor :inputs, :container

    def initialize_system(initializer = Initializer)
      @container = initializer.call(inputs)
    end

    def extract_input_from(options)
      @inputs =  {
        config: options['config'].dup,
        type: options['type'].dup,
        env: options['env'].dup,
        filter: options['filter'].dup,
        template: options['template'].dup
      }
    end

    def elastic_dump
      ElasticDump.run(container, inputs)
    end
  end
end
