module ElasticDump
  class Initializer
    class << self
      DEFAULT_ENV = :dev.freeze
      DEFAULT_CONF = 'config/config.yml'.freeze
      DEFAULT_OUTPUT = 'private/reports'.freeze

      def call(options)
        container    = Dry.initialize_container
        env          = get_env(options)
        config       = explode_config(options, env)
        filter       = options[:filter]
        template       = options[:template]
        output_path  = options[:output_path] || DEFAULT_OUTPUT
        logger       = start_logger
        command_type = options[:type].to_sym || nil

        container.register(:env, env)
        container.register(:config, config)
        container.register(:filter, filter)
        container.register(:output_path, output_path)
        container.register(:logger, logger)
        container.register(:command_type, command_type)
        container.register(:template, template)

        container
      end

      private
      def get_env(options)
        env_app = options[:env] || DEFAULT_ENV
        env_app.downcase.to_sym
      end

      def explode_config(options, env)
        config_path = options[:config] || DEFAULT_CONF
        YAML.load_file(config_path)[env.to_s]
      end

      def start_logger
        logger       = Logger.new(STDOUT)
        logger.level = Logger::INFO
        logger
      end
    end
  end
end
